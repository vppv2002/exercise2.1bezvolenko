package com.company;
import java.util.Locale;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        String Privat = "ПриватБанк";
        String Oshad = "ОщадБанк";
        String PUMB = "ПУМБ";
        float coursePrivatBank = 28.5f;
        float courseOshadBank = 28.65f;
        float coursePUMB = 28.5f;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите количесвто денег, которое хотите обменять: ");
        int amount = Integer.parseInt(scan.nextLine());
        System.out.println("Введите банк через который хотите совершить обмен (ПриватБанк, ОщадБанк или ПУМБ): ");
        String bank = scan.nextLine();
        if (Privat.equalsIgnoreCase(bank)) {
            System.out.println(String.format(Locale.US, "Ваши деньги: %.2f", amount / coursePrivatBank));
        } else if (Oshad.equalsIgnoreCase(bank)) {
            System.out.println(String.format(Locale.US, "Ваши деньги: %.2f", amount / courseOshadBank));
        } else if (PUMB.equalsIgnoreCase(bank)) {
            System.out.println(String.format(Locale.US, "Ваши деньги: %.2f", amount / coursePUMB));
        } else {
            System.err.println("Невозможно сделать обмен " + bank);
        }
    }
}